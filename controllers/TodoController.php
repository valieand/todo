<?php

namespace App\Controllers;

use App\App\App;
use App\App\Request;
use App\App\Session;
use App\Models\Todo;
use App\Widgets\Alert;

class TodoController
{
    public static function index()
    {
        $todos = Todo::findAll();
        $title = 'Todos';

        return view('todos.index', compact('todos', 'title'));
    }

    public static function create()
    {
        $model = new ToDo;

        if (Request::isPost() && $model->load(self::getPost())->validate()) {
            try {
                $model->is_modified = 0;
                $model->insert();
                Alert::success('Task has been saved.');
                return redirect('');
            } catch (\Exception $e) {
                require "views/500.php";
                return;
            }
        } else {
            Alert::errors($model->getErrors());
        }

        $title = 'Create task';
        return view('todos.create', compact('title', 'model'));
    }

    public static function update()
    {
        if (!Session::getInstance()->admin) {
            require "views/404.php";
            return;
        }

        $id = (int) $_GET['id'];

        if (Request::isPost() && $id) {
            $model = new ToDo(self::getPost());
        } else {
            $model = ToDo::findOne($id);
            if (!$model) {
                require "views/404.php";
                return;
            }
        }

        if (Request::isPost() && $model->load(array_merge(['id' => $id], self::getPost()))->validate()) {
            try {
                $dbModel = ToDo::findOne($id);
                $model->is_modified = $model->text !== $dbModel->text ? 1 : $dbModel->is_modified;
                $model->update();
                return redirect('');
            } catch (\Exception $e) {
                require "views/500.php";
                return;
            }
        } else {
            Alert::errors($model->getErrors());
        }

        $title = 'Update task';
        return view('todos.update', compact('title', 'model'));
    }

    public static function delete()
    {
        if (!Session::getInstance()->admin) {
            require "views/404.php";
            return;
        }

        $model = ToDo::findOne((int) $_GET['id']);
        if (!$model) {
            require "views/404.php";
            return;
        }

        try {
            $model->delete();
            return redirect('');
        } catch (\Exception $e) {
            require "views/500.php";
            return;
        }

        return redirect('');
    }

    private function getPost()
    {
        return [
            'email'   => $_POST['email'] ?? null,
            'name'    => $_POST['name'] ?? null,
            'text'    => $_POST['text'] ?? null,
            'is_done' => (int) !!($_POST['is_done'] ?? 0),
        ];
    }
}
