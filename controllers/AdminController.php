<?php

namespace App\Controllers;

use App\App\App;
use App\App\Request;
use App\App\Session;
use App\Models\User;
use App\Widgets\Alert;

class AdminController
{
    public function signin()
    {
        if (Session::getInstance()->admin) {
            require "views/404.php";
            return;
        }

        $data = Session::getInstance();
        $model = new User($this->getPost());

        if (Request::isPost() && $model->validate()) {
            $data->admin = true;
            return redirect('');
        } elseif (Request::isPost()) {
            Alert::error('Invalid credentials');
        }

        $title = 'Sign In';
        return view('admin.signin', compact('title', 'model'));
    }

    public function logout()
    {
        if (!Session::getInstance()->admin) {
            require "views/404.php";
            return;
        }

        $data = Session::getInstance();
        $data->admin = false;
        return redirect('');
    }

    private function getPost()
    {
        return [
            'login' => $_POST['login'] ?? null,
            'password' => $_POST['password'] ?? null,
        ];
    }
}
