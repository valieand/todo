<?php

return [
    'database' => [
        'dbname' => 'todo',
        'username' => 'root',
        'password' => '1234',
        'driver' => 'mysql',
        'host' => 'todo_db', // '127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ],
    ],
    'routes' => [
        'GET' => [
            ['', 'TodoController@index'],
            ['signin', 'AdminController@signin'],
            ['logout', 'AdminController@logout'],
            ['create', 'TodoController@create'],
            ['update', 'TodoController@update'],
            ['delete', 'TodoController@delete'],
        ],
        'POST' => [
            ['create', 'TodoController@create'],
            ['update', 'TodoController@update'],
            ['signin', 'AdminController@signin'],
        ],
    ],
    'params' => [
        'credentials' => [
            'login' => 'admin',
            'password' => '123',
        ],
    ],
];
