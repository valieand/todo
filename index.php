<?php

require 'app/bootstrap.php';

use App\App\Router;
use App\App\Request;

Router::init()->direct(
    Request::uri(),
    Request::method()
);
