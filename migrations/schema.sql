CREATE TABLE `todos` (
  `id` int(11) NOT NULL,
  `is_done` int(1) NOT NULL,
  `is_modified` int(1) NOT NULL DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `todos` ADD PRIMARY KEY (`id`);
ALTER TABLE `todos` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
