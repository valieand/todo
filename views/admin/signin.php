<?php require 'views/layouts/top.php' ?>

<section>

    <h1>Sign in</h1>

    <?= App\Widgets\Alert::flash() ?>

    <form action="/signin" method="post">
      <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" value="<?=$model->login?>">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" value="<?=$model->password?>">
      </div>
      <button type="submit" class="btn btn-primary">Sign In</button>
    </form>

</section>

<?php require 'views/layouts/bottom.php' ?>
