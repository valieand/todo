<div class="bg-light">
  <div class="container pl-0 pr-0">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="/">PHP Todo MVC</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">

          <li class="nav-item"><a class="nav-link" href="/">Todos</a></li>

          <?php if (App\App\Session::getInstance()->admin) { ?>
            <li class="nav-item"><a class="nav-link" href="/logout">Logout</a></li>
          <?php } else { ?>
            <li class="nav-item"><a class="nav-link" href="/signin">Sign In</a></li>
          <?php } ?>
        </ul>
      </div>
    </nav>
  </div>
</div>
