<?php require 'views/layouts/top.php' ?>

<h1>Create new task</h1>

<?php $action = '/create'; ?>
<?php require 'form.php' ?>

<?php require 'views/layouts/bottom.php' ?>
