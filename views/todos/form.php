<section>
    <?= App\Widgets\Alert::flash() ?>

    <form action="<?= $action ?>" method="post">
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" value="<?=$model->email?>">
      </div>
      <div class="form-group">
        <label for="name">Name</label>
        <input type="name" class="form-control" id="name" name="name" value="<?=htmlspecialchars($model->name)?>">
      </div>
      <div class="form-group">
        <label for="text">Text</label>
        <textarea type="text" class="form-control" id="text" name="text" rows="3"><?=htmlspecialchars($model->text)?></textarea>
      </div>
      <?php if(App\App\Session::getInstance()->admin) { ?>
        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="is_done" name="is_done" <?=$model->is_done ? 'checked="checked"' : ''?>>
          <label class="form-check-label" for="is_done">Completed</label>
        </div>
      <?php } ?>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</section>
