<?php require 'views/layouts/top.php' ?>

<?= App\Widgets\Alert::flash() ?>

<h1>Todos</h1>
<p><a class="btn btn-primary" href="/create" role="button">Create new task</a></p>

<?php
    $isAdmin = App\App\Session::getInstance()->admin;

    $columns = [
        '#',
        'Name',
        'Email',
        'Task',
        'Status',
    ];
    if($isAdmin) {
      $columns[] = 'Actions';
    }

    $data = array_map(function ($model) use($isAdmin) {
        $arr = [
            $model->id,
            htmlspecialchars($model->name),
            $model->email,
            htmlspecialchars($model->text) . ($model->is_modified ? '<p class="text-danger">Modified by admin</p>': ''),
            ($model->is_done ? "Done" : ""),
        ];
        if($isAdmin) {
          $arr[] = "<a class='btn btn-light' href='/update?id={$model->id}' role='button'>Edit</a>
          <a class='btn btn-light' href='/delete?id={$model->id}' role='button'>Delete</a>";
        }
        return $arr;
    }, $todos);

    echo (new App\Widgets\Tabulator([
        'options' => ['class' => 'table'],
        'columns' => $columns,
        'data' => $data,
    ]))->render()
?>

<?php require 'views/layouts/bottom.php' ?>
