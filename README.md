# PHP MVC TODO

This is a simple PHP To-do MVC app written for BeeJee as a test todo during recruitment process.

# Setup

## 1. Build

With nginx-proxy (https://bitbucket.org/valieand/nginx-proxy/src/master/):
~~~
docker-compose up -d --build
~~~

Without nginx-proxy, app is available at 127.0.0.1:8099
~~~
docker-compose --file docker-compose.local.yml up -d --build
~~~

## 2. Init app
~~~
docker-compose exec php composer install
~~~

## 3. Create scheme
~~~
cat ./migrations/schema.sql | docker exec -i todo_todo_db_1 /usr/bin/mysql -uroot --password=1234 --default-character-set=utf8 todo
~~~

# License

Forever free.
