<?php

namespace App\App;

class App
{
    protected static $registry = [];

    public static function set(string $key, $val)
    {
        static::$registry[$key] = $val;
    }

    public static function get(string $key)
    {
        return static::$registry[$key];
    }
}
