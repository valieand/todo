<?php

namespace App\App\Database;
use \PDO;

class QueryBuilder
{
    protected $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function selectAll(string $table, string $fetchClass=null)
    {
        $query = $this->db->prepare("select * from {$table};");
        $query->execute();

        if ($fetchClass) {
            return $query->fetchAll(PDO::FETCH_CLASS, $fetchClass);
        }

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function selectOne(string $table, int $id, string $fetchClass=null)
    {
        $query = $this->db->prepare("select * from {$table} where id={$id};");
        $query->execute();

        if ($fetchClass) {
            $result = $query->fetchAll(PDO::FETCH_CLASS, $fetchClass);
            return $result[0] ?? null;
        }

        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result[0] ?? null;
    }

    public function insert(string $table, array $parameters)
    {
        $sql = sprintf(
                "INSERT INTO %s (%s) VALUES (%s)",
                $table,
                implode(', ', array_keys($parameters)),
                ':' . implode(', :', array_keys($parameters))
        );
        $query = $this->db->prepare($sql);
        $query->execute($parameters);
    }

    public function update(string $table, int $id, array $parameters)
    {
        $sql = sprintf(
                "UPDATE %s SET %s WHERE id=%s",
                $table,
                implode(', ', array_map(function ($item) {
                    return $item . '=:' . $item;
                }, array_keys($parameters))),
                $id
        );
        $query = $this->db->prepare($sql);
        $query->execute($parameters);
    }

    public function delete(string $table, int $id)
    {
        $sql = sprintf(
                "DELETE FROM %s WHERE id=%s",
                $table,
                $id
        );
        $query = $this->db->prepare($sql);
        $query->execute($parameters);
    }

}
