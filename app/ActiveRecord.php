<?php

namespace App\App;

class ActiveRecord extends Model
{
    public static function findAll()
    {
        return App::get('db')->selectAll(static::getTable(), static::class);
    }

    public static function findOne(int $id)
    {
        return App::get('db')->selectOne(static::getTable(), $id, static::class);
    }

    public function insert()
    {
        App::get('db')->insert(static::getTable(), $this->getAttributes());
    }

    public function update()
    {
        App::get('db')->update(static::getTable(), $this->id, $this->getAttributes());
    }

    public function delete()
    {
        App::get('db')->delete(static::getTable(), $this->id);
    }
}
