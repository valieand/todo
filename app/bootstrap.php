<?php

use App\App\App;
use App\App\Database\QueryBuilder;
use App\App\Database\Connection;

require 'vendor/autoload.php';

function view(string $viewName, $context=[])
{
    extract($context);
    $filePath = str_replace('.', '/', $viewName);
    require "views/{$filePath}.php";
}

function redirect(string $endpoint)
{
    header("Location: /{$endpoint}");
}

App::set('config', require 'config.php');
App::set('db', new QueryBuilder(Connection::make(App::get('config')['database'])));
