<?php

namespace App\App;

class Model
{
    public function __construct(array $attributes = [])
    {
        $this->load($attributes);
    }

    private $errors = [];

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Method should be implemented in child classes
     * @return array Rules array
     */
    public function rules()
    {
        return [];
    }

    public function validate()
    {
        $rules = $this->rules();

        $result = true;
        foreach ($rules as $attribute => $rule) {
            if (!$rule['validator']->validate($this->$attribute)) {
                $result = false;
                $this->errors[] = $rule['message'] ?? "Invalid {$attribute}";
            }
        }
        return $result;
    }

    public function load(array $attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->$attribute = $value;
        }

        return $this;
    }
}
