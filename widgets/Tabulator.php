<?php

namespace App\Widgets;

class Tabulator extends Table
{
    const PAGE_LENGTH = 5;

    public function render()
    {
        $this->options['id'] = $this->options['id'] ?? 'id_' . uniqid();

        $options = [
            'lengthChange' => false,
            'lengthMenu' => [self::PAGE_LENGTH],
            'paging' => count($this->data) > self::PAGE_LENGTH,
            'searching' => false,
        ];

        return '
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.css"/>
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script>
        ' . parent::render() . '<script>
            $(document).ready(function() {
                $("#' . $this->options['id'] . '").DataTable(' . json_encode($options) . ');
            } );
        </script>';
    }
}
