<?php

namespace App\Widgets;

class Table extends \App\App\Model
{
    public $columns;
    public $data;
    public $options;

    public function render()
    {
        $options = implode(' ', array_map(function ($value, $key) {
            return "{$key}='{$value}'";
        }, $this->options, array_keys($this->options)));

        return "<table {$options}>" . $this->renderHeader() . $this->renderBody() . "</table>";
    }

    private function renderHeader()
    {
        return '<thead><tr>' . array_reduce($this->columns, function ($carry, $item) {
            return $carry . "<th scope='col'>{$item}</th>";
        }) . '</tr></thead>';
    }

    private function renderBody()
    {
        return '<tbody>' . array_reduce($this->data, function ($carry, $item) {
            return $carry . '<tr>' . array_reduce($item, function ($carry, $item) {
                return $carry . "<td>{$item}</td>";
            }) . '</tr>';
        }) . '</tbody>';
    }
}
