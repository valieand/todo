<?php

namespace App\Widgets;

use App\App\Session;

class Alert
{
    public static function errors(array $errors)
    {
        foreach ($errors as $error) {
            static::error($error);
        }
    }

    public static function error(string $text)
    {
        $data = Session::getInstance();
        if (!$data->errors) {
            $data->errors = [];
        }
        $data->errors = array_merge($data->errors, [$text]);
    }

    public static function success(string $text)
    {
        $data = Session::getInstance();
        if (!$data->successes) {
            $data->successes = [];
        }
        $data->successes = array_merge($data->successes, [$text]);
    }

    public static function flash()
    {
        $data = Session::getInstance();

        foreach ($data->errors ?? [] as $text) {
            echo "<div class='alert alert-danger' role='alert'>{$text}<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        }

        foreach ($data->successes ?? [] as $text) {
            echo "<div class='alert alert-success' role='alert'>{$text}<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        }

        $data->errors = [];
        $data->successes = [];
    }
}
