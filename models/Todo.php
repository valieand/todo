<?php

namespace App\Models;

use Respect\Validation\Validator as v;

class Todo extends \App\App\ActiveRecord
{
    public $id;
    public $email;
    public $name;
    public $text;
    public $is_done;
    public $is_modified;

    public function rules()
    {
        return [
            'email' => [
                'validator' => v::email(),
                'message' => 'Invalid email',
            ],
            'name' => [
                'validator' => v::stringVal()->length(1, 255),
                'message' => 'Invalid name',
            ],
            'text' => [
                'validator' => v::stringVal()->length(1, 255),
                'message' => 'Invalid task',
            ],
            'is_done' => [
                'validator' => v::in([0, 1]),
                'message' => 'Invalid is_done',
            ],
        ];
    }

    public static function getTable()
    {
        return 'todos';
    }

    public function getAttributes()
    {
        return [
            'email' => $this->email,
            'name' => $this->name,
            'text' => $this->text,
            'is_done' => $this->is_done,
            'is_modified' => $this->is_modified,
        ];
    }
}
