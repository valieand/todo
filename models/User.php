<?php

namespace App\Models;

use App\App\App;
use Respect\Validation\Validator as v;

class User extends \App\App\Model
{
    public $login;
    public $password;

    public function rules()
    {
        $credentials = App::get('config')['params']['credentials'];

        return [
            'login' => [
                'validator' => v::equals($credentials['login']),
                'message' => 'Invalid login',
            ],
            'password' => [
                'validator' => v::equals($credentials['password']),
                'message' => 'Invalid password',
            ],
        ];
    }
}
